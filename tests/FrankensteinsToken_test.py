from web3 import Web3
from brownie import FrankensteinsToken, accounts, reverts
import pytest


@pytest.fixture
def token(FrankensteinsToken):
    token = accounts[0].deploy(FrankensteinsToken)
    return token


# workaround for handling revert with Custom Errors
def err(name):
    return "typed error: " + Web3.keccak(text=name)[:4].hex()


def test_deployment():
    accounts[0].deploy(FrankensteinsToken)


def test_erc20_totalSupply(token):
    assert token.totalSupply() == 10 * 100


def test_erc20_balanceOf(token):
    assert token.balanceOf(accounts[0]) == 10 * 100
    assert token.balanceOf(accounts[1]) == 0
    token.safeTransferFrom(accounts[0], accounts[1], 1, 20, "")
    assert token.balanceOf(accounts[0]) == 10 * 100 - 20


def test_erc20_transfer(token):
    # transfer less than balance
    tx = token.transfer(accounts[1], 50)
    assert "Transfer" in tx.events
    assert tx.events["Transfer"]["from"] == accounts[0]
    assert tx.events["Transfer"]["to"] == accounts[1]
    assert tx.events["Transfer"]["value"] == 50
    assert "TransferBatch" in tx.events
    assert token.balanceOf(accounts[0]) == 10 * 50
    assert token.balanceOf(accounts[1]) == 10 * 50

    # transfer more than balance
    with reverts(err("NotEnoughBalance()")):
        token.transfer(accounts[1], 100)

    # transfer more than single token ID, but less than others
    token.safeTransferFrom(accounts[0], accounts[1], 1, 25, "")
    tx = token.transfer(accounts[1], 30)
    assert "Transfer" in tx.events
    assert tx.events["Transfer"]["from"] == accounts[0]
    assert tx.events["Transfer"]["to"] == accounts[1]
    assert tx.events["Transfer"]["value"] == 30
    assert "TransferBatch" in tx.events
    assert token.balanceOf(accounts[0]) == 10 * 50 - 9 * 30 - 50
    assert token.balanceOf(accounts[1]) == 10 * 50 + 9 * 30 + 50
    assert token.balanceOf(accounts[0], 1) == 0
    assert token.balanceOf(accounts[1], 1) == 100
    assert token.balanceOf(accounts[0], 2) == 20
    assert token.balanceOf(accounts[1], 2) == 80


def test_erc20_transferFrom(token):
    token.transferFrom(accounts[0], accounts[1], 100)

    with reverts(err("NotApproved()")):
        token.transferFrom(accounts[1], accounts[0], 100)

    token.approve(accounts[0], 50, {"from": accounts[1]})
    with reverts(err("NotApproved()")):
        token.transferFrom(accounts[1], accounts[0], 100)
    token.transferFrom(accounts[1], accounts[0], 50)
    with reverts(err("NotApproved()")):
        token.transferFrom(accounts[1], accounts[0], 1)

    token.approve(accounts[0], 50, {"from": accounts[1]})
    token.transferFrom(accounts[1], accounts[0], 25)
    token.transferFrom(accounts[1], accounts[0], 25)
    with reverts(err("NotApproved()")):
        token.transferFrom(accounts[1], accounts[0], 1)


def test_erc20_approval(token):
    assert token.allowance(accounts[0], accounts[1]) == 0
    tx = token.approve(accounts[1], 100)
    assert "Approval" in tx.events
    assert tx.events["Approval"]["owner"] == accounts[0]
    assert tx.events["Approval"]["spender"] == accounts[1]
    assert tx.events["Approval"]["value"] == 100
    assert token.allowance(accounts[0], accounts[1]) == 100
    token.transferFrom(accounts[0], accounts[1], 25, {"from": accounts[1]})
    assert token.allowance(accounts[0], accounts[1]) == 75
