# Image for building and running tests in CI

FROM debian:bullseye
LABEL maintainer="kacper.zuk@codepoets.it"
RUN apt-get update && apt-get install -y \
    build-essential \
    nodejs npm \
    python3 python3-dev python3-pip \
    && rm -rf /var/lib/apt/lists/*
RUN useradd -mU user
RUN pip3 install --no-cache-dir eth-brownie slither-analyzer black py-solc-x
RUN npm install -g ganache
USER user
# install latest solc - will probably speed up Ci
RUN python3 -c "import solcx; solcx.install_solc()"
