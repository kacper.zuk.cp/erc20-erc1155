#!/bin/bash

# make sure we're at project's root directory
cd "$(dirname "$0")/.."

# print out commands that are being run + make sure we don't silently ignore errors
set -exuo pipefail

echo "Running solidity code formatter"
npx prettier --write 'contracts/**/*.sol'

echo "Running solidity static code analyzer"
slither . --filter-paths "packages/" --exclude solc-version,erc721-interface

echo "Running python code formatter"
black tests/ scripts/

echo "Running application tests"
brownie test
