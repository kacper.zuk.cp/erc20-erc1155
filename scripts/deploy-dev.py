from brownie import FrankensteinsToken, accounts


def main():
    token = FrankensteinsToken.deploy({"from": accounts[0]})
    print(token)
