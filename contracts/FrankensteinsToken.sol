// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";
import "@openzeppelin/contracts/token/ERC1155/extensions/ERC1155Supply.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/utils/math/Math.sol";

/// @title A token that combines ERC20 and ERC1155
/// @notice ERC20 methods behave as ERC1155 batch methods over all token ids
contract FrankensteinsToken is ERC1155, ERC1155Supply, IERC20 {
    /** @dev owner => spender => allowance
     *
     * Tracks allowance for ERC20 transferFrom.  This is completely separate
     * from ERC1155 approvalForAll
     */
    mapping(address => mapping(address => uint256)) allowancesERC20;

    uint8 constant NUMBER_OF_TOKEN_IDS = 10;
    uint8 constant PREMINT_AMOUNT = 100;

    error NotEnoughBalance();
    error NotApproved();

    /// @dev Mints 10 token IDs with 100 tokens each and sends to contract's creator
    constructor() ERC1155("asset.") {
        uint256[] memory ids = new uint256[](NUMBER_OF_TOKEN_IDS);
        uint256[] memory vals = new uint256[](NUMBER_OF_TOKEN_IDS);
        for (uint8 i = 1; i <= NUMBER_OF_TOKEN_IDS; ++i) {
            ids[i - 1] = i;
            vals[i - 1] = PREMINT_AMOUNT;
        }
        _mintBatch(msg.sender, ids, vals, "");
    }

    function _beforeTokenTransfer(
        address operator,
        address from,
        address to,
        uint256[] memory ids,
        uint256[] memory amounts,
        bytes memory data
    ) internal override(ERC1155, ERC1155Supply) {
        super._beforeTokenTransfer(operator, from, to, ids, amounts, data);
    }

    /// @notice ERC20 totalSupply()
    /// @return Sum of supply over all ERC1155 token IDs
    function totalSupply() external view returns (uint256) {
        uint256 supply = 0;
        for (uint8 id = 1; id <= NUMBER_OF_TOKEN_IDS; ++id) {
            supply += totalSupply(id);
        }
        return supply;
    }

    /// @notice ERC20 balanceOf(address account)
    /// @return Sum of balance over all ERC1155 token IDs for given account
    function balanceOf(address account) external view returns (uint256) {
        uint256 balance = 0;
        for (uint8 id = 1; id <= NUMBER_OF_TOKEN_IDS; ++id) {
            balance += balanceOf(account, id);
        }
        return balance;
    }

    /** @dev Helper function for translating ERC20 transfer to ERC1155 batch transfer
     *
     * ERC20 transfers transfer a given amount of all token IDs. If a given
     * account doesn't have enough balance of a token ID, whole balance will be
     * sent out. If none token ID has enough balance to transfer given amount,
     * tx will be reverted.
     * This function generates ERC1155 batch transfer `ids` and `amounts` arrays
     * that follow the rules above.
     */
    function _transferParamsERC20(address from, uint256 amount)
        internal
        view
        returns (uint256[] memory, uint256[] memory)
    {
        uint256[] memory ids = new uint256[](NUMBER_OF_TOKEN_IDS);
        uint256[] memory amounts = new uint256[](NUMBER_OF_TOKEN_IDS);
        uint256 maxTransfer = 0;

        for (uint8 id = 1; id <= NUMBER_OF_TOKEN_IDS; ++id) {
            uint8 index = id - 1;
            ids[index] = id;

            // if not enough balance, transfer out everything
            amounts[index] = Math.min(amount, balanceOf(from, id));

            // track to check if at least one token ID has enough balance
            maxTransfer = Math.max(maxTransfer, amounts[index]);
        }

        if (maxTransfer != amount) {
            revert NotEnoughBalance();
        }

        return (ids, amounts);
    }

    /** @notice ERC20 transferFrom(address from, address to, uint256 amount)
     *
     * Allowance for ERC20 functions is tracked completely separately from
     * ERC1155. transferFrom will reduce allowance by the amount specified in the
     * call, even if some token IDs didn't have enough balance.
     *
     * @dev See also _transferParamsERC20
     */
    function transferFrom(
        address from,
        address to,
        uint256 amount
    ) public returns (bool) {
        // if caller isn't the owner, it must have enough allowance
        if (from != msg.sender) {
            if (allowancesERC20[from][msg.sender] < amount) {
                revert NotApproved();
            }
            unchecked {
                allowancesERC20[from][msg.sender] -= amount;
            }
        }

        (uint256[] memory ids, uint256[] memory amounts) = _transferParamsERC20(
            from,
            amount
        );

        _safeBatchTransferFrom(from, to, ids, amounts, "");
        emit Transfer(from, to, amount);
        return true;
    }

    /// @notice ERC20 transfer(address to, uint256 amount)
    function transfer(address to, uint256 amount) external returns (bool) {
        return transferFrom(msg.sender, to, amount);
    }

    /// @notice ERC20 allowance(address owner, address spender)
    function allowance(address owner, address spender)
        external
        view
        returns (uint256)
    {
        return allowancesERC20[owner][spender];
    }

    /// @notice ERC20 approve(address spender, uint256 amount)
    function approve(address spender, uint256 amount) external returns (bool) {
        allowancesERC20[msg.sender][spender] = amount;
        emit Approval(msg.sender, spender, amount);
        return true;
    }
}
