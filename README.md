# ERC20/ERC1155 Frankenstein's Token

## Token description

This is an [OpenZeppelin's ERC1155 token](https://docs.openzeppelin.com/contracts/4.x/erc1155) with added implementation of basic ERC20 interface. ERC20 methods work as batch operations over all token IDs in a best effort way. Implemented ERC20 methods:

* `totalSupply()`: sum of total supplies for all token ids
* `balanceOf(account)`: sum of given account's balances for all token ids
* `transfer(to, amount)`: transfer `amount` amount from caller to `to` account from all token ids each. This is done in a "best effort" way. If caller has less than `amount` of a given token ID, whole balance will be transferred. Requires at least one token ID to have enough balance to transfer `amount`, reverts otherwise. Emits both ERC20 `Transfer` event and ERC1155 `TransferBatch` event.
* `transferFrom(from, to, amount)`: like above, but can transfer from any account as long as enough allowance is approved. Allowance is tracked separately from ERC1155's `ApprovalForAll`.
* `allowance(owner, spender)`: returns the current allowance - how much can `spender` transfer on behalf of `owner`. Allowance is tracked separately from ERC1155's `ApprovalForAll`.
* `approve(spender, amount)`: approves `spender` to transfer up to `amount` on behalf of the caller.Allowance is tracked separately from ERC1155's `ApprovalForAll`.
* event `Transfer(from, to, value)`: emitted only when transfer is done via ERC20 interface. Value is equal to the maximum amount of any token ID transferred.
* event `Approval(owner, spender, value)`: emitted only for approval done via ERC20 interface.

Please note that it doesn't implement the optional `name`, `symbol` and `decimals` optional methods from ERC20

## Tools

* [Brownie](https://eth-brownie.readthedocs.io/en/stable/index.html) - smart contract development framework
* [Slither](https://github.com/crytic/slither) - solidity static code analyzer
* [Prettier](https://prettier.io/) with [Solidity plugin](https://github.com/prettier-solidity/prettier-plugin-solidity) - solidity code formatter
* [Black](https://github.com/psf/black) - Python code formatter
* [Visual Studio Code](https://code.visualstudio.com/) (optional)

## Setup your env

Make sure you have Python3.8+ and Node.js v12+ with NPM installed.

Install pipx (used to install brownie later):

```bash
apt install python3-venv python3-dev pipx build-essential
pipx ensurepath
```

If you're on non-apt distro or your distro doesn't have pipx in repo, make sure
you have venv module for your py3 and basic build toolchain for py3 extensions
and then install pipx with:

```bash
python3 -m pip install --user pipx
python3 -m pipx ensurepath
```

Install brownie, slither and black:

```bash
pipx install eth-brownie slither-analyzer black
```

Install ganache:

```bash
npm install -g ganache
# or if you don't want to install system-wide (which requires root)
npm install --global --prefix $HOME/.npm ganache # add $HOME/.npm/bin to your $PATH
```

Install prettier:

```bash
# in project's root directory
npm install
```

### Visual Studio Code

Install extensions:

* [ms-python.python](https://marketplace.visualstudio.com/items?itemName=ms-python.python)
* [JuanBlanco.solidity](https://marketplace.visualstudio.com/items?itemName=JuanBlanco.solidity)
* [trailofbits.slither-vscode](https://marketplace.visualstudio.com/items?itemName=trailofbits.slither-vscode)
* [esbenp.prettier-vscode](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)

Hit Ctrl-Shift-P, enter `Python: Select Interpreter` and provide the interpreter
used by Brownie. If you installed with pipx as recommended above, that would be
`~/.local/pipx/venvs/eth-brownie/bin/python`.

### Other editors

You may want to integrate Prettier, Black and Slither into your editor, so that
you're notified about potential issues as soon as they're written.

## Running formatters/linters/analyzers/tests

You're expected to run this and make sure there are no issues before opening a pull request:

```bash
./scripts/precommit.sh
```

This command runs:

* code formatting using Prettier for Solidity
* static code analysis using Slither for Solidity
* code formatting using Black for Python
* application tests using Brownie

Please note that Slither's `solc-version` detector is disabled. It recommends against using most recent versions of solc compiler which is a direct contradiction of official Solidity recommendation:

> When deploying contracts, you should use the latest released version of Solidity.

Source: <https://docs.soliditylang.org/en/v0.8.12/>
